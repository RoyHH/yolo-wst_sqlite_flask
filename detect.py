import argparse

from models import *  # set ONNX_EXPORT in models.py
from utils.datasets import *
from utils.utils import *

import datetime as dt


from Yolo_to_Sqlite import *
from multiprocessing import Process


def Make_Dir(image_dir):
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)

def detect(infolder_path, SN_id, save_img=False):
    imgsz = (320, 192) if ONNX_EXPORT else opt.img_size  # (320, 192) or (416, 256) or (608, 352) for (height, width)


    '''================================== Flask Sqlite ================================'''
    if goto_Sqlite == 'Done':
        out, weights, half, view_img, save_txt = opt.output, opt.weights, opt.half, opt.view_img, opt.save_txt
        source = infolder_path
    else:
        out, source, weights, half, view_img, save_txt = opt.output, opt.source, opt.weights, opt.half, opt.view_img, opt.save_txt


    webcam = source == '0' or source.startswith('rtsp') or source.startswith('http') or source.endswith('.txt')

    # Initialize
    device = torch_utils.select_device(device='cpu' if ONNX_EXPORT else opt.device)

    # # clear output folder or not
    # if os.path.exists(out):
    #     shutil.rmtree(out)  # delete output folder
    # os.makedirs(out)  # make new output folder

    # Initialize model
    model = Darknet(opt.cfg, imgsz)

    # Load weights
    attempt_download(weights)
    if weights.endswith('.pt'):  # pytorch format
        model.load_state_dict(torch.load(weights, map_location=device)['model'])
    else:  # darknet format
        load_darknet_weights(model, weights)

    # Second-stage classifier
    classify = False
    if classify:
        modelc = torch_utils.load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()

    # Eval mode
    model.to(device).eval()

    # Fuse Conv2d + BatchNorm2d layers
    # model.fuse()

    # Export mode
    if ONNX_EXPORT:
        model.fuse()
        img = torch.zeros((1, 3) + imgsz)  # (1, 3, 320, 192)
        f = opt.weights.replace(opt.weights.split('.')[-1], 'onnx')  # *.onnx filename
        torch.onnx.export(model, img, f, verbose=False, opset_version=11,
                          input_names=['images'], output_names=['classes', 'boxes'])

        # Validate exported model
        import onnx
        model = onnx.load(f)  # Load the ONNX model
        onnx.checker.check_model(model)  # Check that the IR is well formed
        print(onnx.helper.printable_graph(model.graph))  # Print a human readable representation of the graph
        return

    # Half precision
    half = half and device.type != 'cpu'  # half precision only supported on CUDA
    if half:
        model.half()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        torch.backends.cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz)

    # Get names and colors
    names = load_classes(opt.names)
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img.float()) if device.type != 'cpu' else None  # run once


    '''================================== Flask Sqlite ================================'''
    if goto_Sqlite == 'Done':
        img_num = 0   # count for images
        object_num = 0   # count for objects
        # cls_sta = torch.zeros(len(names))   # statistics head data
        cls_sta = torch.zeros(len(names), device=device)   # statistics head data
        outfolder_time = dt.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')   # time_id: file folder name


    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)


        '''============ Flask Sqlite ============'''
        if goto_Sqlite == 'Done':
            img_num += 1


        # Inference
        t1 = torch_utils.time_synchronized()
        pred = model(img, augment=opt.augment)[0]
        t2 = torch_utils.time_synchronized()

        # to float
        if half:
            pred = pred.float()

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres,
                                   multi_label=False, classes=opt.classes, agnostic=opt.agnostic_nms)

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        for i, det in enumerate(pred):  # detections for image i
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            # save_path = str(Path(out) / Path(p).name)
            Make_Dir(str(Path(out) / outfolder_time))
            save_path = str(Path(out) / outfolder_time / Path(p).name)


            '''============ Flask Sqlite ============'''
            if goto_Sqlite == 'Done':
                table_data_sta = dict()


            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  #  normalization gain whwh
            if det is not None and len(det):
                # Rescale boxes from imgsz to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string


                    '''============ Flask Sqlite ============'''
                    if goto_Sqlite == 'Done':
                        if img_num <= dataset.nF:
                            cls_sta[int(c)] += n

                    '''============ Flask ============'''
                    if goto_Flask == 'Done':
                        with open(save_path[:save_path.rfind('.')] + '.txt', 'a') as file:
                            file.write(('# %s( %g ) \n' % (names[int(c)], n)))  # label format


                # Write results
                for *xyxy, conf, cls in reversed(det):
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        with open(save_path[:save_path.rfind('.')] + '.txt', 'a') as file:
                            file.write(('%g ' * 5 + '\n') % (cls, *xywh))  # label format

                    if save_img or view_img:  # Add bbox to image
                        label = '%s %.2f' % (names[int(cls)], conf)
                        plot_one_box(xyxy, im0, label=label, color=colors[int(cls)])


                    '''================================== Flask + Flask Sqlite ================================'''
                    if goto_Flask == 'Done' or goto_Sqlite == 'Done':
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        relative_xywh = xyxy2xywh(torch.tensor(xyxy).view(1, 4)).view(-1)
                        absolute_area = relative_xywh[-2] * relative_xywh[-1]
                        relative_area =absolute_area / (gn[-2] * gn[-1])
                        relative_xywh.tolist()
                        absolute_area.tolist()
                        relative_area.tolist()
                        with open(save_path[:save_path.rfind('.')] + '.txt', 'a') as file:
                            file.write(('%s:' + ' %g' * 5 + '\n') % (names[int(cls)], *relative_xywh, relative_area))

                        '''================================== Flask Sqlite ================================'''
                        if goto_Sqlite == 'Done':
                            object_num += 1
                            table_data = {"id": str(object_num), "ImgNum": str(img_num), "class": names[int(cls)],
                                          "X": str(int(relative_xywh[0])), "Y": str(int(relative_xywh[1])),
                                          "W": str(int(relative_xywh[2])), "H": str(int(relative_xywh[3])),
                                          "P": str(float(relative_area))}
                            table_data_sta.update({"tabledata"+str(object_num): table_data})

            '''================================== Flask Sqlite ================================'''
            if goto_Sqlite == 'Done':
                with open(save_path[:save_path.rfind('.')] + '.txt', 'a') as file:
                    file.write('@\n%s\n' % (table_data_sta))
                ''' Sqlite---table_data '''
                date_id = outfolder_time[:outfolder_time.find('_')]
                time_id = outfolder_time[outfolder_time.find('_'):]
                file_name = Path(p).name[:Path(p).name.rfind('.')]
                cur_idx.execute("INSERT INTO Img_tb (from_path, date_id, time_id, SN_id, img_num, img_name, Info) "
                                "values(?, ?, ?, ?, ?, ?, ?)",
                                (source, date_id, time_id, SN_id, str(img_num), file_name, str(table_data_sta)))
                con_sql.commit()


            # Print time (inference + NMS)
            print('%sDone. (%.3fs)' % (s, t2 - t1))

            # Stream results
            if view_img:
                cv2.imshow(p, im0)
                if cv2.waitKey(1) == ord('q'):  # q to quit
                    raise StopIteration

            # Save results (image with detections)
            if save_img:
                if dataset.mode == 'images':
                    cv2.imwrite(save_path, im0)
                else:
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer

                        fps = vid_cap.get(cv2.CAP_PROP_FPS)
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*opt.fourcc), fps, (w, h))
                    vid_writer.write(im0)


    '''================================== Flask Sqlite ================================'''
    if goto_Sqlite == 'Done':
        if img_num == dataset.nF:
            head_data_save_path = str(Path(out) / outfolder_time) + '/head_data.txt'
            if goto_Flask == 'Done':
                with open(head_data_save_path, 'a') as file:
                    head_data = dict()
                    for i in range(len(names)):
                        head_data.update({names[i]: int(cls_sta[i])})
                        file.write(('%s: %g  \n' % (names[i], cls_sta[i])))
                    head_data_sta = {"sta": head_data}
                    file.write('@\n%s\n' % (head_data_sta))
                    cur_idx_head.execute("INSERT INTO Head_tb (from_path, date_id, time_id, SN_id, Info) "
                                         "values(?, ?, ?, ?, ?)",
                                         (source, date_id, time_id, SN_id, str(head_data_sta)))
                    con_sql.commit()

                    fileNum = 0
                    for lists in os.listdir(source):
                        In_path = os.path.join(source, lists)
                        print('Detect_InPath:  ', In_path)
                        if os.path.isfile(In_path):
                            fileNum = fileNum + 1
                    cimg_num = fileNum
                    out_path = out + '/' + outfolder_time
                    cur_idx_outfolder.execute("INSERT INTO OutFolder_tb (from_path, out_path, date_id, time_id, SN_id, "
                                              "cimg_num) values(?, ?, ?, ?, ?, ?)",
                                              (source, out_path, date_id, time_id, SN_id, cimg_num))
                    con_sql.commit()


    '''================================== Flask Sqlite ================================'''
    if clear_Img_tb == 'Done':
        cur_idx.execute("DELETE FROM Img_tb  ")  # clear table
        con_sql.commit()


    if save_txt or save_img:
        print('Results saved to %s' % os.getcwd() + os.sep + out)
        if platform == 'darwin':  # MacOS
            os.system('open ' + save_path)

    print('Done. (%.3fs)' % (time.time() - t0))


''' 实时监控——从Sqlite中查询未detect的新文件，喂入detect '''
def Real_Time_Monitoring_Detect(opr_folder_num_id, wait_num):
    cur_idx_infolder.execute("select * from InFolder_tb ORDER BY id DESC LIMIT 1")
    folder_result = cur_idx_infolder.fetchone()
    cur_folder_num_id = folder_result[5]
    # print("\n\033[0;32;40m cur_folder_num_id ======> \033[0m", (cur_folder_num_id))
    # print("\n\033[0;34;40m opr_folder_num_id ======> \033[0m", (opr_folder_num_id))
    print("\n\033[0;32;40m cur_folder_num_id ======> \033[0m %s  "
          "  \033[0;34;40m opr_folder_num_id ======> \033[0m %s"
          % (cur_folder_num_id, opr_folder_num_id))

    if opr_folder_num_id < cur_folder_num_id:
        opr_folder_num_id += 1

        cur_idx_infolder.execute("select * from InFolder_tb WHERE id= %s" % (opr_folder_num_id))
        folder_result = cur_idx_infolder.fetchone()
        print("\nfolder_result", folder_result)
        opr_folder = folder_result[0]
        opr_folder_Sn_id = folder_result[3]

        with torch.no_grad():
            detect(opr_folder, opr_folder_Sn_id)

        wait_num = D_wait_num
        Real_Time_Monitoring_Detect(opr_folder_num_id, wait_num)

    else:
        # 等待时间
        time.sleep(D_wait_time)
        wait_num = wait_num - 1
        if wait_num != 0:
            print("\n\033[0;33m detect wait -------- \033[0m")
            Real_Time_Monitoring_Detect(opr_folder_num_id, wait_num)
        else:
            print("\n\033[0;33m ******** Detect Stop, No new folder ******** \033[0m")


''' 实时监控——新文件录入Sqlite '''
def Real_Time__Monitoring_Folder(folder_path, wait_num):
    new_folder, SN_id = new_report(folder_path)
    cur_idx_infolder.execute("select * from InFolder_tb ORDER BY id DESC LIMIT 1")
    folder_result = cur_idx_infolder.fetchone()
    cur_folder= folder_result[0]
    # print("\n\033[0;31;40m new_folder ======> \033[0m", (new_folder))
    # print("\n\033[0;32;40m cur_folder ======> \033[0m", (cur_folder))
    print("\n\033[0;31;40m new_folder ======> \033[0m %s  "
          "  \033[0;32;40m cur_folder ======> \033[0m %s"
          % (new_folder, cur_folder))

    if new_folder != cur_folder:
        cur_folder = new_folder
        cur_folder_time = dt.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')  # time_id: file folder name
        cur_folder_date_id = cur_folder_time[:cur_folder_time.find('_')]
        cur_folder_time_id = cur_folder_time[cur_folder_time.find('_'):]
        fileNum = 0
        # 统计 new_folder 下的文件数量
        for lists in os.listdir(cur_folder):
            sub_path = cur_folder + '/' + lists
            print('Monitoring_Folder:  ', sub_path)
            if os.path.isfile(sub_path):
                fileNum = fileNum + 1
        cimg_num = fileNum
        cur_idx_infolder.execute("INSERT INTO InFolder_tb (folder_name, date_id, time_id, SN_id, cimg_num) "
                                 "values(?, ?, ?, ?, ?)",
                                 (cur_folder, cur_folder_date_id, cur_folder_time_id, SN_id, cimg_num))
        con_sql.commit()

        wait_num = S_wait_num
        Real_Time__Monitoring_Folder(folder_path, wait_num)

    else:
        # 等待时间
        time.sleep(S_wait_time)
        wait_num = wait_num - 1
        if wait_num != 0:
            print("\n\033[0;33m sqlite wait -------- \033[0m")
            Real_Time__Monitoring_Folder(folder_path, wait_num)
        else:
            print("\n\033[0;33m ******** Sqlite Stop, No new folder ******** \033[0m")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    '''================================== 判别是否依照前端修改相关参数 ================================'''
    if getmes_default:
        parser.add_argument('--cfg', type=str, default=Default_cfg, help='*.cfg path')
        parser.add_argument('--weights', type=str, default=Default_weights, help='weights path')
        parser.add_argument('--source', type=str, default=Default_source, help='source')  # input file/folder, 0 for webcam
        parser.add_argument('--output', type=str, default=Default_output, help='output folder')  # output folder
        parser.add_argument('--conf-thres', type=float, default=Default_conf_thres, help='object confidence threshold')
        parser.add_argument('--iou-thres', type=float, default=Default_iou_thres, help='IOU threshold for NMS')
        parser.add_argument('--device', default=Default_device, help='device id (i.e. 0 or 0,1) or cpu')
    else:
        cur_idx_cfg.execute("select * from Flask_cfg_tb ORDER BY id DESC LIMIT 1")
        Flask_cfg = cur_idx_cfg.fetchone()[0]
        cur_idx_weights.execute("select * from Flask_weights_tb ORDER BY id DESC LIMIT 1")
        Flask_weights = cur_idx_weights.fetchone()[0]
        cur_idx_source.execute("select * from Flask_source_tb ORDER BY id DESC LIMIT 1")
        Flask_source = cur_idx_source.fetchone()[0]
        cur_idx_output.execute("select * from Flask_output_tb ORDER BY id DESC LIMIT 1")
        Flask_output = cur_idx_output.fetchone()[0]
        cur_idx_thres.execute("select * from Flask_thres_tb ORDER BY id DESC LIMIT 1")
        Flask_thres_results = cur_idx_thres.fetchone()
        Flask_conf_thres = Flask_thres_results[0]
        Flask_iou_thres = Flask_thres_results[1]
        print("\n\033[0;35m Flask_conf_thres in Sqlite ------------------- \033[0m", Flask_conf_thres)
        print("\n\033[0;35m Flask_iou_thres in Sqlite ------------------- \033[0m", Flask_iou_thres)
        cur_idx_device.execute("select * from Flask_device_tb ORDER BY id DESC LIMIT 1")
        Flask_device = cur_idx_device.fetchone()[0]
        parser.add_argument('--cfg', type=str, default=Flask_cfg, help='*.cfg path')
        parser.add_argument('--weights', type=str, default=Flask_weights, help='weights path')
        parser.add_argument('--source', type=str, default=Flask_source, help='source')  # input file/folder, 0 for webcam
        parser.add_argument('--output', type=str, default=Flask_output, help='output folder')  # output folder
        parser.add_argument('--conf-thres', type=float, default=Flask_conf_thres, help='object confidence threshold')
        parser.add_argument('--iou-thres', type=float, default=Flask_iou_thres, help='IOU threshold for NMS')
        parser.add_argument('--device', default=Flask_device, help='device id (i.e. 0 or 0,1) or cpu')

    # parser.add_argument('--cfg', type=str, default='cfg/yolov3.cfg', help='*.cfg path')
    # parser.add_argument('--weights', type=str, default='weights/0924-0.5115-best_d300-yolov3-smallset-iou0.2.pt', help='weights path')
    # parser.add_argument('--source', type=str, default='data/samples', help='source')  # input file/folder, 0 for webcam
    # parser.add_argument('--output', type=str, default='output', help='output folder')  # output folder
    # parser.add_argument('--conf-thres', type=float, default=0.3, help='object confidence threshold')
    # parser.add_argument('--iou-thres', type=float, default=0.6, help='IOU threshold for NMS')
    # parser.add_argument('--device', default='', help='device id (i.e. 0 or 0,1) or cpu')
    parser.add_argument('--names', type=str, default='data/template.names', help='*.names path')
    parser.add_argument('--img-size', type=int, default=512, help='inference size (pixels)')
    parser.add_argument('--fourcc', type=str, default='mp4v', help='output video codec (verify ffmpeg support)')
    parser.add_argument('--half', action='store_true', help='half precision FP16 inference')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    opt = parser.parse_args()
    opt.cfg = check_file(opt.cfg)  # check file
    opt.names = check_file(opt.names)  # check file
    print(opt)

    # with torch.no_grad():
    #     detect()

    if goto_Sqlite == 'Done':
        ''' ================ 预运行 —— 启动时，按当前最新文件进行detect ================ '''
        cur_folder, SN_id = new_report(opt.source)
        opr_folder = cur_folder
        opr_folder_time = dt.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')  # time_id: file folder name
        opr_folder_date_id = opr_folder_time[:opr_folder_time.find('_')]
        opr_folder_time_id = opr_folder_time[opr_folder_time.find('_'):]
        fileNum = 0
        # 统计 new_folder 下的文件数量
        for lists in os.listdir(opr_folder):
            sub_path = opr_folder + '/' + lists
            print('ProRead_Folder:  ', sub_path)
            if os.path.isfile(sub_path):
                fileNum = fileNum+1
        cimg_num = fileNum
        cur_idx_infolder.execute("INSERT INTO InFolder_tb (folder_name, date_id, time_id, SN_id, cimg_num) "
                                 "values(?, ?, ?, ?, ?)",
                                 (opr_folder, opr_folder_date_id, opr_folder_time_id, SN_id, cimg_num))
        con_sql.commit()

        ''' ================ 持续运行 —— 实时统计最新文件、当前正detect的文件和待detect的文件，持续进行detect ================ '''
        # 获取当前sqlite中folder的最新id，作为最初要处理的文件，放置监控正detect的文件探针——opr_folder_num_id
        cur_idx_infolder.execute("select * from InFolder_tb ORDER BY id DESC LIMIT 1")
        folder_result = cur_idx_infolder.fetchone()
        cur_folder_num_id = folder_result[5]
        print("\n\033[0;32m cur_folder_num_id ======> \033[0m", (cur_folder_num_id))
        opr_folder_num_id = cur_folder_num_id
        # opr_folder_num_id = 8   # Debug时用

        with torch.no_grad():
            detect(opr_folder, SN_id)

            # 多线程（副程序），实时监控——新文件录入Sqlite
            p_folder_sqlite = Process(target=Real_Time__Monitoring_Folder, args=(opt.source, S_wait_num,))
            p_folder_sqlite.start()

            # （主程序）实时监控——从Sqlite中查询未detect的新文件，喂入detect
            Real_Time_Monitoring_Detect(opr_folder_num_id, D_wait_num)


    else:
        with torch.no_grad():
            detect()
