### **1.功能逻辑**
![Image](data/some to Readme/111.png)

详细情况参见思维导图 
```
data/some to Readme/软件功能划分.xmind
```
***

### **3.文件说明**
- [detect.py](./detect.py)：detect 功能
- [train.py](./train.py)：train 功能
- [Yolo_to_Sqlite.py](./Yolo_to_Sqlite.py)：Sqlite 使用功能，以及 Algorithm——Sqlite——Flask 链条中所有超参信息和相关函数模块 
- [Yolo_to_Flask_YC.py](./Yolo_to_Flask_YC.py)：发送、接收信号和命令
***





    
