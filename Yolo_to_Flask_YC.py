from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
import requests
from train import *
import time
from Yolo_to_Sqlite import *
from multiprocessing import Process
import base64
import cv2
import numpy as np
import ast

app = Flask(__name__)
CORS(app)
#cors_1 = CORS(app, resources={r"/getMetrics": {"origins": "*"}})

names = locals()
date_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
             '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
             '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
             '31']

def get(url):
  try:
    res = requests.get(url)
    return res.json()
  except:
    return False

def sendimg(url, i):
    #将图片数据转成base64格式
    url = 'C:/Users/YC/Desktop/vue-test/IGBT/output/' + url + '.jpg'
    #print('url =', url)
    with open(url, 'rb') as f:
        img = base64.b64encode(f.read()).decode()
    image = []
    image.append(img)
    res = {"image":image, "num":i}
    #访问服务
    _ = requests.post("http://127.0.0.1:8000/getimg",data=res)
    #print('send一张图')


''' ========================== 执行 ========================== '''
def YoloDetect(go):
    print('YoloDetect——os.getcwd()=', os.getcwd())
    #os.chdir(r"C:/Users/YC/Desktop/vue-test/IGBT_DF_Roy-20210926")
    (status, output) = subprocess.getstatusoutput('python detect.py')
    print('status = ', status)
    print('output = ', output)

# 待解决，这个启动函数会报错： UnicodeDecodeError: 'gbk' codec can't decode byte 0x8b in position 14203: illegal multibyte sequence
def YoloTrain(go):
    print('YoloTrain——os.getcwd()=', os.getcwd())
    (status, output) = subprocess.getstatusoutput('python train.py')
    print('status = ', status)
    print('output = ', output)

def Real_Time_Monitoring_OutFolder(opr_out_folder_from, opr_out_folder_cnum):
    des_all = dict()
    for img_n in range(opr_out_folder_cnum):
        cur_idx.execute("select * from Img_tb WHERE from_path= '%s' and img_num= '%s'" % (opr_out_folder_from, str(img_n+1)))
        for column_i in cur_idx.fetchall():
            des_temp_v = column_i[6]
        # des_all.update({str(img_n+1): des_temp_v})
        if img_n == 0:
            des_all = des_temp_v[1:-1]
        else:
            if des_temp_v != '{}':
                des_all = des_all + ',' + des_temp_v[1:-1]
    while des_all[0] == ',':
        des_all = des_all[1:]

    cur_idx_time.execute("select * from Img_tb WHERE from_path= '%s' and img_num= '%s'" % (opr_out_folder_from, str(img_n + 1)))
    for column_i in cur_idx_time.fetchall():
        date_id = column_i[1]
        time_id = column_i[2]
    date_time = date_id + time_id

    for img_n in range(opr_out_folder_cnum):
        cur_idx_name.execute("select * from Img_tb WHERE from_path= '%s' and img_num= '%s'" % (opr_out_folder_from, str(img_n + 1)))
        for column_i in cur_idx_name.fetchall():
            outpath_temp_v = date_time + '/' + column_i[5]
        # des_all.update({str(img_n+1): des_temp_v})
        if img_n == 0:
            outpath_all = outpath_temp_v
        else:
            if outpath_temp_v != '{}':
                outpath_all = outpath_all + ',' + outpath_temp_v

    cur_idx_head.execute("select * from Head_tb WHERE from_path= '%s'" % (opr_out_folder_from))
    for column_i in cur_idx_head.fetchall():
        sta = column_i[4]

    cur_idx_outfolder.execute("select * from OutFolder_tb ORDER BY id DESC LIMIT 1")
    cur_out_result = cur_idx_outfolder.fetchone()
    cur_out_folder_id = cur_out_result[6]

    return des_all, sta, cur_out_folder_id, outpath_all

def Ask_Search_Result(NS, Tk, VSD, VST, SK, VSS, PK, VSP):
    print("params ===", NS, Tk, VSD, VST, SK, VSS, PK, VSP)
    if NS:
        if Tk == "True":
            cur_idx.execute("select * from Img_tb WHERE date_id= '%s' and time_id= '%s'" % (VSD, VST))
        elif SK == "True":
            cur_idx.execute("select * from Img_tb WHERE SN_id= '%s'" % (VSS))
        elif PK == "True":
            cur_idx.execute("select * from Img_tb WHERE from_path= '%s'" % (VSP))
        # cur_idx.execute("select * from Img_tb WHERE from_path= '%s'" % (VSP))
        for column_i in cur_idx.fetchall():
            from_folder = column_i[0]
        cur_idx_infolder.execute("select * from InFolder_tb WHERE folder_name= '%s'" % (from_folder))  #####
        for column_i in cur_idx_infolder.fetchall():
            cimg_num = column_i[4]

        des_all, sta, id, outpath_all = Real_Time_Monitoring_OutFolder(from_folder, cimg_num)  #####
        print("\n\033[0;34m Ask_Search_Result：des_all ======> \033[0m", des_all)
        print("\n\033[0;34m Ask_Search_Result：sta ======> \033[0m", sta)
        return des_all, sta, id, outpath_all

    else:
        time.sleep(2)
        Ask_Search_Result(NS, Tk, VSD, VST, SK, VSS, PK, VSP)

def Ask_Setting(GM, CK, Cs, WK, Ws, SK, Ss, OK, Os, TK, Tcs, Tis, DK, Ds):
    if GM == "True":
        if CK == "True":
            print("\n\033[0;31m Flask_cfg in Sqlite ------------------- \033[0m")
            cur_idx_cfg.execute("INSERT INTO Flask_cfg_tb (Flask_cfg) values(?)", (Cs,))
            con_sql.commit()
        if WK == "True":
            print("\n\033[0;32m Flask_weights in Sqlite ------------------- \033[0m")
            cur_idx_weights.execute("INSERT INTO Flask_weights_tb (Flask_weights) values(?)", (Ws,))
            con_sql.commit()
        if SK == "True":
            print("\n\033[0;33m Flask_source in Sqlite ------------------- \033[0m")
            cur_idx_source.execute("INSERT INTO Flask_source_tb (Flask_source) values(?)", (Ss,))
            con_sql.commit()
        if OK == "True":
            print("\n\033[0;34m Flask_output in Sqlite ------------------- \033[0m")
            cur_idx_output.execute("INSERT INTO Flask_output_tb (Flask_output) values(?)", (Os,))
            con_sql.commit()
        if TK == "True":
            print("\n\033[0;35m Flask_thres in Sqlite ------------------- \033[0m")
            cur_idx_thres.execute("INSERT INTO Flask_thres_tb (Flask_conf_thres, Flask_iou_thres) "
                                  "values(?, ?)", (Tcs, Tis))
            con_sql.commit()
        if DK == "True":
            print("\n\033[0;36m Flask_device in Sqlite ------------------- \033[0m")
            cur_idx_device.execute("INSERT INTO Flask_device_tb (Flask_device) values(?)", (Ds,))
            con_sql.commit()

    else:
        cur_idx_cfg.execute("INSERT INTO Flask_cfg_tb (Flask_cfg) values(?)", (Default_cfg,))
        con_sql.commit()
        cur_idx_weights.execute("INSERT INTO Flask_weights_tb (Flask_weights) values(?)", (Default_weights,))
        con_sql.commit()
        cur_idx_source.execute("INSERT INTO Flask_source_tb (Flask_source) values(?)", (Default_source,))
        con_sql.commit()
        cur_idx_output.execute("INSERT INTO Flask_output_tb (Flask_output) values(?)", (Default_output,))
        con_sql.commit()
        cur_idx_thres.execute("INSERT INTO Flask_thres_tb (Flask_conf_thres, Flask_iou_thres) "
                              "values(?, ?)", (Default_conf_thres, Default_iou_thres))
        con_sql.commit()
        cur_idx_device.execute("INSERT INTO Flask_device_tb (Flask_device) values(?)", (Default_device,))
        con_sql.commit()

def Ask_Setting_Train(TGM, TEK, TEs, TBK, TBs, TCK, TCs, TWK, TWs, TNK, TNs, TDK, TDs):
    if TGM == "True":
        if TEK == "True":
            print("\n\033[0;31m T_Flask_epochs in Sqlite ------------------- \033[0m")
            cur_idx_T_epochs.execute("INSERT INTO T_Flask_epochs_tb (T_Flask_epochs) values(?)", (TEs,))
            con_sql.commit()
        if TBK == "True":
            print("\n\033[0;32m T_Flask_batchsize in Sqlite ------------------- \033[0m")
            cur_idx_T_batchsize.execute("INSERT INTO T_Flask_batchsize_tb (T_Flask_batchsize) values(?)", (TBs,))
            con_sql.commit()
        if TCK == "True":
            print("\n\033[0;33m T_Flask_cfg in Sqlite ------------------- \033[0m")
            cur_idx_T_cfg.execute("INSERT INTO T_Flask_cfg_tb (T_Flask_cfg) values(?)", (TCs,))
            con_sql.commit()
        if TWK == "True":
            print("\n\033[0;34m T_Flask_weights in Sqlite ------------------- \033[0m")
            cur_idx_T_weights.execute("INSERT INTO T_Flask_weights_tb (T_Flask_weights) values(?)", (TWs,))
            con_sql.commit()
        if TNK == "True":
            print("\n\033[0;35m T_Flask_name in Sqlite ------------------- \033[0m")
            cur_idx_T_name.execute("INSERT INTO T_Flask_name_tb (T_Flask_name) values(?)", (TNs,))
            con_sql.commit()
        if TDK == "True":
            print("\n\033[0;36m T_Flask_device in Sqlite ------------------- \033[0m")
            cur_idx_T_device.execute("INSERT INTO T_Flask_device_tb (T_Flask_device) values(?)", (TDs,))
            con_sql.commit()

    else:
        cur_idx_T_epochs.execute("INSERT INTO T_Flask_epochs_tb (T_Flask_epochs) values(?)", (Train_Default_epochs,))
        con_sql.commit()
        cur_idx_T_batchsize.execute("INSERT INTO T_Flask_batchsize_tb (T_Flask_batchsize) values(?)", (Train_Default_batchsize,))
        con_sql.commit()
        cur_idx_T_cfg.execute("INSERT INTO T_Flask_cfg_tb (T_Flask_cfg) values(?)", (Train_Default_cfg,))
        con_sql.commit()
        cur_idx_T_weights.execute("INSERT INTO T_Flask_weights_tb (T_Flask_weights) values(?)", (Train_Default_weights,))
        con_sql.commit()
        cur_idx_T_name.execute("INSERT INTO T_Flask_name_tb (T_Flask_name) values(?)", (Train_Default_name,))
        con_sql.commit()
        cur_idx_T_device.execute("INSERT INTO T_Flask_device_tb (T_Flask_device) values(?)", (Train_Default_device,))
        con_sql.commit()

def Ask_Statistic_allclass(Sd):
    cur_idx_head.execute("select * from Head_tb ORDER BY id DESC LIMIT 1")
    temp_result = cur_idx_head.fetchone()
    temp_id = temp_result[5]
    temp_date = temp_result[1]
    month_id = temp_date[5:7]
    print("month_id ==============", month_id)
    print("temp_id ==============", temp_id)
    # Sum_sta = {'hair': 0, 'hairs': 0, 'fiber': 0, 'fibers': 0, 'spot': 0, 'foreign': 0,
    #            'uneven': 0, 'fixed support': 0, 'nut': 0}
    Sum_sta = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    if Sd:
        temp_Sd = Sd[8:]
        sd = date_list.index(temp_Sd, 0, 30)
        # cur_idx_head.execute("select * from Head_tb WHERE date_id= '%s'" % (Sd))
        # temp_result = cur_idx_head.fetchone()
    else:
        sd = 0

    for i in range(31):
        date_keyb = temp_date[:8] + date_list[sd + i]
        cur_idx_head.execute("select * from Head_tb WHERE date_id= '%s'" % (date_keyb))
        temp_b_result = cur_idx_head.fetchone()
        if temp_b_result:
            temp_b_id = temp_b_result[5]
            print("temp_b_id ==============", temp_b_id)
            n_c = temp_id - temp_b_id
            for i_c in range(n_c + 1):
                cur_idx_head.execute("select Info from Head_tb WHERE id= %s" % (temp_b_id + i_c))
                con_temp = cur_idx_head.fetchone()
                con_temp_dict = ast.literal_eval(list(con_temp)[0])

                Sum_sta[0] += con_temp_dict['sta']['hair']
                Sum_sta[1] += con_temp_dict['sta']['hairs']
                Sum_sta[2] += con_temp_dict['sta']['fiber']
                Sum_sta[3] += con_temp_dict['sta']['fibers']
                Sum_sta[4] += con_temp_dict['sta']['spot']
                Sum_sta[5] += con_temp_dict['sta']['foreign']
                Sum_sta[6] += con_temp_dict['sta']['uneven']
                Sum_sta[7] += con_temp_dict['sta']['fixed support']
                Sum_sta[8] += con_temp_dict['sta']['nut']
            print("Sum_sta ==============", Sum_sta)
            break
    return Sum_sta


''' ========================== 用于解读 TXT to Flask ========================== '''
def read_results_one(result_path, fileName):   # 单个识别结果txt读取处理
    descrption = dict()
    statistics = dict()
    with open(result_path + fileName, 'r') as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            line = line.strip()

            if line.find('#') == 0:
                findNumber_0 = line.find('(')
                findNumber_00 = line.find(')')
                statistics.update({line[2:findNumber_0]: line[findNumber_0+1:findNumber_00]})
            else:
                findNumber_n = line.find(':')
                descrption.update({line[0:findNumber_n]: line[findNumber_n + 2:]})

    stat = {'Sta': statistics}
    desc = {'Des': descrption}
    print("statistics = %s \ndescrption = %s \n\n " % (statistics, descrption))
    print("Sta = %s \nDes = %s \n\n " % (stat, desc))
    return stat, desc

def read_Metric_results(result_path, fileName):
    Mt = dict()
    with open(result_path + fileName, 'r') as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            line = line.strip()
            findNumber = line.find(':')
            Mt.update({line[0:findNumber]: line[findNumber + 2:]})

    Mtc = {'Metric': Mt}
    print("Mt = %s \n" % (Mt))
    print("Metric = %s \n" % (Mtc))
    return Mtc


'''
Real_Time_Monitor 实时侦测模块
YOLO_flask接口：sendMsg
已完成	 
'''
@app.route('/sendMsg', methods=['GET', 'POST'])
def home():
    p_detect = Process(target=YoloDetect, args=(1,))
    p_detect.start()

    if Read_TXT:
        ''' txt文件解析 '''
        result_path = "output/"
        fileName = "5.txt"
        result_pre, ext = os.path.splitext(fileName)
        if ext == '.txt':
            sta, dsc = read_results_one(result_path, fileName)
            fileName = fileName[0]
            #sta = {'Sta': {'hair': ' 5 ', 'fiber': ' 2 ', 'spot': ' 1 ', 'foreign': ' 1 '}}
            #dsc = {'Des': {'hair': '901.5 1221 159 86 0.00275614', 'fiber': '1045.5 267 303 206 0.012581', 'foreign': '738.5 446 139 74 0.00207326'}}
            #                  类别:  x,    y,   w,  h, 异物全图占比

    if Read_Sqlite:
        ''' =============== Sqlite解析 --- Real_Time =============== '''
        cur_idx_outfolder.execute("select * from OutFolder_tb ORDER BY id DESC LIMIT 1")
        cur_out_result = cur_idx_outfolder.fetchone()
        cur_out_folder_id = cur_out_result[6]
        cur_out_folder_cnum = cur_out_result[5]
        cur_out_folder_from = cur_out_result[0]
        print("\n\033[0;32;40m cur_out_folder_id ======> \033[0m", (cur_out_folder_id))
        opr_out_folder_id = cur_out_folder_id
        opr_out_folder_cnum = cur_out_folder_cnum
        opr_out_folder_from = cur_out_folder_from

        des_all, sta, cur_out_folder_id, outpath_all = Real_Time_Monitoring_OutFolder(opr_out_folder_from, opr_out_folder_cnum)
        print("\n\033[0;31m Read_Sqlite：des_all ======> \033[0m", des_all)
        print("\n\033[0;31m Read_Sqlite：sta ======> \033[0m", sta)

        outpath_list = outpath_all.split(",")
        print(outpath_list,type(outpath_list))
        for i in range(len(outpath_list)):
            sendimg(outpath_list[i], i+1)
        return jsonify(sta, cur_out_folder_id, des_all, outpath_all)
    # return jsonify(sta, dsc, img_num)


''' 
Ask_Search_Result 搜索数据模块
YOLO_flask接口：sendSerMsg
已完成	 
'''
@app.route('/sendSerMsg', methods=['GET', 'POST'])
def sendSerMsg():
    NowSearch = True
    TimeKey = False
    Vue_Search_DateKey = "2022-03-02"
    Vue_Search_TimeKey = "_14_30_17"
    SNKey = False
    Vue_Search_SNKey = "xxxxxxxxxxxxxx"
    PathKey = True
    Vue_Search_PathKey = "data/samples/333"

    data_all = {"data_all": request.get_json(silent=True)}
    if data_all != None:
        print("data_all = ", data_all)
        NowSearch = str(request.form['NowSearch'])
        TimeKey = str(request.form['TimeKey'])
        Vue_Search_DateKey = str(request.form['Vue_Search_DateKey'])
        Vue_Search_TimeKey = str(request.form['Vue_Search_TimeKey'])
        SNKey = str(request.form['SNKey'])
        Vue_Search_SNKey = str(request.form['Vue_Search_SNKey'])
        PathKey = str(request.form['PathKey'])
        Vue_Search_PathKey = str(request.form['Vue_Search_PathKey'])
        print(NowSearch,TimeKey,Vue_Search_DateKey,Vue_Search_TimeKey,SNKey,Vue_Search_SNKey,PathKey,Vue_Search_PathKey)

    des_all, sta, id, outpath_all = Ask_Search_Result(NowSearch,
                                                      TimeKey, Vue_Search_DateKey, Vue_Search_TimeKey,
                                                      SNKey, Vue_Search_SNKey,
                                                      PathKey, Vue_Search_PathKey)

    outpath_list = outpath_all.split(",")
    print(outpath_list, type(outpath_list))
    for i in range(len(outpath_list)):
        sendimg(outpath_list[i], i + 1)
    return jsonify(sta, id, des_all, outpath_all)


'''
Ask_Setting_Detect 侦测设置模块
YOLO_flask接口：getSetDet
已完成
'''
@app.route('/getSetDet', methods=['GET', 'POST'])
def getSetDet():
    getmes_from_Flask_key = "True"
    Flask_cfg_key = "True"
    Flask_cfg_st = 'cfg/yolov3.cfg'
    Flask_weights_key = "True"
    Flask_weights_st = 'weights/0924-0.5115-best_d300-yolov3-smallset-iou0.2.pt'
    Flask_source_key = "True"
    Flask_source_st = 'data/samples'
    Flask_output_key = "True"
    Flask_output_st = 'output'
    Flask_thres_key = "True"
    Flask_conf_thres_st = 0.5
    Flask_iou_thres_st = 0.5
    Flask_device_key = "True"
    Flask_device_st = ''

    # data_all = {"data_all": request.get_json(silent=True)}
    # print("data_all = ", data_all)
    # getmes_from_Flask_key = data_all["data_all"]["params"]["getmes_from_Flask_key"]
    # Flask_cfg_key = data_all["data_all"]["params"]["Flask_cfg_key"]
    # Flask_cfg_st = data_all["data_all"]["params"]["Flask_cfg_st"]
    # Flask_weights_key = data_all["data_all"]["params"]["Flask_weights_key"]
    # Flask_weights_st = data_all["data_all"]["params"]["Flask_weights_st"]
    # Flask_source_key = data_all["data_all"]["params"]["Flask_source_key"]
    # Flask_source_st = data_all["data_all"]["params"]["Flask_source_st"]
    # Flask_output_key = data_all["data_all"]["params"]["Flask_output_key"]
    # Flask_output_st = data_all["data_all"]["params"]["Flask_output_st"]
    # Flask_thres_key = data_all["data_all"]["params"]["Flask_thres_key"]
    # Flask_conf_thres_st = data_all["data_all"]["params"]["Flask_conf_thres_st"]
    # Flask_iou_thres_st = data_all["data_all"]["params"]["Flask_iou_thres_st"]
    # Flask_device_key = data_all["data_all"]["params"]["Flask_device_key"]
    # Flask_device_st = data_all["data_all"]["params"]["Flask_device_st"]

    Ask_Setting(getmes_from_Flask_key,
                Flask_cfg_key, Flask_cfg_st,
                Flask_weights_key, Flask_weights_st,
                Flask_source_key, Flask_source_st,
                Flask_output_key, Flask_output_st,
                Flask_thres_key, Flask_conf_thres_st, Flask_iou_thres_st,
                Flask_device_key, Flask_device_st)
    print("设置成功")
    return jsonify("设置成功")


'''
Ask_Monthly_Count 月度统计模块
YOLO_flask接口：sendMolCot
完成
输入为：单个日期字符串。例如：'2022-03-22'
需要输出一行以 [ hair, hairs, fiber, fibers, spot, foreign, uneven, fixed_support, nut] 为顺序的list，
例如[100, None, 100, 90, 80, 100,  50, 60, None]
'''
@app.route('/sendMolCot', methods=['GET', 'POST'])
def sendMolCot():
    data_all = {"data_all": request.get_json(silent=True)}
    if data_all != None:
        print("data_all = ", data_all)
        if str(request.form['GM']) == "True":
            startdate_M = str(request.form['startdate_M']) #例如：'2022-03-22'

            #此处执行功能函数
            sta_sum = Ask_Statistic_allclass(startdate_M)

            res = {"chartdata_M": sta_sum}
            # res = {"chartdata_M":[100,0,100,100,None,100,50,60,71]}
            return jsonify(res)


'''
Ask_Train_Start 训练启动模块
YOLO_flask接口：getTraSrt
有问题， 待解决
需要输出一行内容为 "训练完成" 的字符串
'''
@app.route('/getTraSrt', methods=['GET', 'POST'])
def getTraSrt():
    data_all = {"data_all": request.get_json(silent=True)}
    if data_all != None:
        print("data_all = ", data_all)
        if str(request.form['GM']) == "True":

            #此处执行功能函数
            p_train = Process(target=YoloTrain, args=(2,))
            p_train.start()

            res = {"mes" : "训练完成"}
            return jsonify(res)


'''
Ask_Yearly_Count 年度统计模块
YOLO_flask接口：sendYelCot
完成
输入为：单个日期字符串。例如：'2022-03-22'
需要输出4个分别命名为chartdata_F1、chartdata_P、chartdata_R、chartdata_mAP的list
    chartdata_F1：一行包括12个月度F1指标的list，
    例如[0.667, 0, 1,  0.901, 0.855, 0,  0.494, 0, 0, 0.10, 0.11, 0.12]
    chartdata_P：一行包括12个月度P指标的list，
    例如[0.667, 0, 1,  0.901, 0.855, 0,  0.494, 0, 0, 0.10, 0.11, 0.12]
    chartdata_R：一行包括12个月度R指标的list，
    例如[0.667, 0, 1,  0.901, 0.855, 0,  0.494, 0, 0, 0.10, 0.11, 0.12]
    chartdata_mAP：一行包括12个月度mAP指标的list，
    例如[0.667, 0, 1,  0.901, 0.855, 0,  0.494, 0, 0, 0.10, 0.11, 0.12]
'''
@app.route('/sendYelCot', methods=['GET', 'POST'])
def sendYelCot():
    data_all = {"data_all": request.get_json(silent=True)}
    if data_all != None:
        print("data_all = ", data_all)
        if str(request.form['GM']) == "True":
            startdate_Y = str(request.form['startdate_Y']) #例如：'2022-03-22'

            #此处执行功能函数
            if startdate_Y:
                cur_idx_metric.execute("select * from Metric_tb WHERE date_id= %s" % (startdate_Y))
                metric_result = cur_idx_metric.fetchone()
                metric_id = metric_result[6]
            else:
                cur_idx_metric.execute("select * from Metric_tb ORDER BY id DESC LIMIT 1")
                metric_result = cur_idx_metric.fetchone()
                metric_id = metric_result[6]

            temp_F1 = [None, None, None, None, None, None, None, None, None, None, None, None]
            temp_P = [None, None, None, None, None, None, None, None, None, None, None, None]
            temp_R = [None, None, None, None, None, None, None, None, None, None, None, None]
            temp_mAP = [None, None, None, None, None, None, None, None, None, None, None, None]

            if metric_id < 12:
                for i in range(metric_id):
                    cur_idx_metric.execute("select * from Metric_tb WHERE id= %s" % (i + 1))
                    metric_result = cur_idx_metric.fetchone()
                    metric_F1 = metric_result[2]
                    metric_P = metric_result[3]
                    metric_R = metric_result[4]
                    metric_mAP = metric_result[5]
                    temp_F1[i] = metric_F1
                    temp_P[i] = metric_P
                    temp_R[i] = metric_R
                    temp_mAP[i] = metric_mAP
                    # print("<12 ===========", (temp_F1, temp_P, temp_R, temp_mAP))
            else:
                for i in range(12):
                    cur_idx_metric.execute("select * from Metric_tb WHERE id= %s" % (metric_id - i))
                    metric_result = cur_idx_metric.fetchone()
                    metric_F1 = metric_result[2]
                    metric_P = metric_result[3]
                    metric_R = metric_result[4]
                    metric_mAP = metric_result[5]
                    temp_F1[11 - i] = metric_F1
                    temp_P[11 - i] = metric_P
                    temp_R[11 - i] = metric_R
                    temp_mAP[11 - i] = metric_mAP
                    # print(">12 ===========", (temp_F1, temp_P, temp_R, temp_mAP))

            res = {"chartdata_F1": temp_F1,
                   "chartdata_P": temp_P,
                   "chartdata_R": temp_R,
                   "chartdata_mAP": temp_mAP,
                   }
            print("res ==========", res)
            return jsonify(res)


"""
Ask_Setting_Train 训练设置模块
YOLO_flask接口：getSetTra
完成
输入为cfg,weights等参数，与另一设置模块类似
需要输出一行内容为 "设置成功" 的字符串
"""
@app.route('/getSetTra', methods=['GET', 'POST'])
def getSetTra():
    Train_getmes_from_Flask_key = "True"
    Train_Flask_epochs = "True"
    Train_Flask_epochs_st = 3
    Train_Flask_batchsize = "True"
    Train_Flask_batchsize_st = 2
    Train_Flask_cfg = "True"
    Train_Flask_cfg_st = 'cfg/yolov3.cfg'
    Train_Flask_weights = "True"
    Train_Flask_weights_st = 'weights/darknet53.conv.74'
    Train_Flask_name = "True"
    Train_Flask_name_st = 'd300'
    Train_Flask_device = "True"
    Train_Flask_device_st = '0'

    """
    #data_all = {"data_all": request.get_json(silent=True)}
    #print("data_all = ", data_all)
    #Train_getmes_from_Flask_key = data_all["data_all"]["params"]["Train_getmes_from_Flask_key"]
    #Train_Flask_epochs = data_all["data_all"]["params"]["Train_Flask_epochs"]
    #Train_Flask_epochs_st = data_all["data_all"]["params"]["Train_Flask_epochs_st"]
    #Train_Flask_batchsize = data_all["data_all"]["params"]["Train_Flask_batchsize"]
    #Train_Flask_batchsize_st = data_all["data_all"]["params"]["Train_Flask_batchsize_st"]
    #Train_Flask_cfg = data_all["data_all"]["params"]["Train_Flask_cfg"]
    #Train_Flask_cfg_st = data_all["data_all"]["params"]["Train_Flask_cfg_st"]
    #Train_Flask_weights = data_all["data_all"]["params"]["Train_Flask_weights"]
    #Train_Flask_weights_st = data_all["data_all"]["params"]["Train_Flask_weights_st"]
    #Train_Flask_name = data_all["data_all"]["params"]["Train_Flask_name"]
    #Train_Flask_name_st = data_all["data_all"]["params"]["Train_Flask_name_st"]
    #Train_Flask_device = data_all["data_all"]["params"]["Train_Flask_device"]
    #Train_Flask_device_st = data_all["data_all"]["params"]["Train_Flask_device_st"]
    """

    #此处执行功能函数
    Ask_Setting_Train(Train_getmes_from_Flask_key,
                      Train_Flask_epochs, Train_Flask_epochs_st,
                      Train_Flask_batchsize, Train_Flask_batchsize_st,
                      Train_Flask_cfg, Train_Flask_cfg_st,
                      Train_Flask_weights, Train_Flask_weights_st,
                      Train_Flask_name, Train_Flask_name_st,
                      Train_Flask_device, Train_Flask_device_st)
    print("设置成功")
    return jsonify("设置成功")


#用于查询detect setting内的各项名单，不属于七接口之一，请忽略
@app.route('/recep4', methods=['GET', 'POST'])
def recep4():
    data_all = {"data_all":request.get_json(silent=True)}["data_all"]
    print("data_all = ",data_all)
    if data_all["params"]["name1"] == "cfg":
            model_dir = data_all["params"]["name1"]
            model_path = "./" + model_dir
            print("model_path = ", model_path)
            print('os.chdir()=', os.getcwd())
            data6 = os.listdir(model_path)
            print("modelsdir = ", data6)
            data4 = {"modelsdir" :data6}
    else:
        data4 = {"modelsdir" : "XXX"}
    #data2 = data_all["data_all"]["data2"]
    #data3 = data_all["data_all"]["data3"]
    #print("data1 = ",data1)
    #print("data2 = ",data2)
    #print("data4 = ",data4)
    return jsonify(data4)


# 启动运行
if __name__ == '__main__':
    # home()    # 我调试时用的，可忽略
    # recep()
    # recep_1()
    # Read_OutFolder_tolist()
    # outfolder = 'output/2022-03-02_18_07_34'
    # OutFolder_File_tolist(outfolder)

    Sd = '2022-03-05'
    sta_sum = Ask_Statistic_allclass(Sd)
    print('sta_sum =============', sta_sum)

    # app.run(host="127.0.0.1", port=7000, debug='True')   # 这样子会直接运行在本地服务器，也即是 localhost:5000
    # app.run(host='your_ip_address') # 这里可通过 host 指定在公网IP上运行

    # result_path = "./output/"
    # fileName = "201116_112012_0000000028_CAM1_NG.txt"
    # result_pre, ext = os.path.splitext(fileName)
    # if ext == '.txt':
    #    sta, dsc = read_results_one(result_path, fileName)

    # result_path = ""
    # fileName = "Results_best.txt"
    # result_pre, ext = os.path.splitext(fileName)
    # if ext == '.txt':
    #     train_metric = read_Metric_results(result_path, fileName)
    #     print("train_metric = %s \n" % (train_metric))

    # temp_time = dt.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')  # time_id: file folder name
    # date_id = temp_time[:temp_time.find('_')]
    # time_id = temp_time[temp_time.find('_'):]
    # metric_temp = dict()
    # with open(Metric_results_file, 'r') as f:
    #     lines = f.readlines()
    #     for i, line in enumerate(lines):
    #         line = line.strip()
    #         name_temp = line.find(':')
    #         values_0 = line.find('(')
    #         values_00 = line.find(')')
    #         metric_temp.update({i: line[values_0 + 1:values_00]})
    # cur_idx_metric.execute("INSERT INTO Metric_tb (date_id, time_id, Precise, Recall, mAP, F1) "
    #                        "values(?, ?, ?, ?, ?, ?)",
    #                        (date_id, time_id, metric_temp[0], metric_temp[1], metric_temp[2], metric_temp[3]))
    # con_sql.commit()

    # cur_idx_T_device.execute("DROP TABLE T_Flask_device_tb")  # clear table
    # con_sql.commit()
    # cur_idx.execute("DELETE FROM Img_tb")  # clear table
    # con_sql.commit()