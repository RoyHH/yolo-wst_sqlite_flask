import sqlite3
import os

''' ===================================== Algorithm——Sqlite 超参数 ===================================== '''
'''
sample 产生的速度快于 detect 时，D_wait_num无用
sample 产生的速度慢于 detect 时，D_wait_num起作用
'''
D_wait_num = 3   # 等待次数
D_wait_time = 10   # 等待时间（s）
S_wait_num = 3   # 等待次数
S_wait_time = 12   # 等待时间（s）
R_wait_num = 3   # 等待次数
R_wait_time = 10   # 等待时间（s）



''' ===================================== Sqlite ===================================== '''
goto_Flask = 'Done'   # 生成 detect 结果的txt文件，供 Flask 调取
goto_Sqlite = 'Done'   # detect 结果直接写入 Sqlite，供 Flask 调取
clear_Img_tb = 'no'   # 清理 Img_tb
'''
Sqlite_Detect.db（库）
Img_tb（用于存储 images 的具体信息）—— cur_idx 为指针
Head_tb（用于存储 一组 images 的统计信息）—— cur_idx_head 为指针
InFolder_tb（用于存储原始图片的文件夹信息）—— cur_idx_infolder 为指针
InFolder_tb（用于存储 detect 后的图片的文件夹信息）—— cur_idx_outfolder 为指针
'''
con_sql = sqlite3.connect('Sqlite_Detect.db')
# con_sql = sqlite3.connect('Sqlite_Detect.db', check_same_thread=False)
''' Sqlite---table_data '''
cur_idx = con_sql.cursor()  # create cursor
Img_tb = "CREATE TABLE IF NOT EXISTS Img_tb (from_path char, date_id char, time_id char, SN_id char, " \
         "img_num char, img_name char, Info char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx.execute(Img_tb)  # open table

''' Sqlite---head_data '''
cur_idx_head = con_sql.cursor()  # create cursor
Head_tb = "CREATE TABLE IF NOT EXISTS Head_tb (from_path char, date_id char, time_id char, SN_id char, " \
          "Info char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_head.execute(Head_tb)  # open table

''' Sqlite---InFolder '''
cur_idx_infolder = con_sql.cursor()  # create cursor
InFolder_tb = "CREATE TABLE IF NOT EXISTS InFolder_tb (folder_name char, date_id char, time_id char, SN_id char, " \
          "cimg_num int, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_infolder.execute(InFolder_tb)  # open table

''' Sqlite---OutFolder '''
cur_idx_outfolder = con_sql.cursor()  # create cursor
OutFolder_tb = "CREATE TABLE IF NOT EXISTS OutFolder_tb (from_path char, out_path char, date_id char, time_id char, " \
               "SN_id char, cimg_num int, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_outfolder.execute(OutFolder_tb)  # open table


''' Sqlite---Flask_cfg '''
cur_idx_cfg = con_sql.cursor()  # create cursor
Flask_cfg_tb = "CREATE TABLE IF NOT EXISTS Flask_cfg_tb (Flask_cfg char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_cfg.execute(Flask_cfg_tb)  # open table

''' Sqlite---Flask_weights '''
cur_idx_weights = con_sql.cursor()  # create cursor
Flask_weights_tb = "CREATE TABLE IF NOT EXISTS Flask_weights_tb (Flask_weights char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_weights.execute(Flask_weights_tb)  # open table

''' Sqlite---Flask_source '''
cur_idx_source = con_sql.cursor()  # create cursor
Flask_source_tb = "CREATE TABLE IF NOT EXISTS Flask_source_tb (Flask_source char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_source.execute(Flask_source_tb)  # open table

''' Sqlite---Flask_output '''
cur_idx_output = con_sql.cursor()  # create cursor
Flask_output_tb = "CREATE TABLE IF NOT EXISTS Flask_output_tb (Flask_output char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_output.execute(Flask_output_tb)  # open table

''' Sqlite---Flask_thres '''
cur_idx_thres = con_sql.cursor()  # create cursor
Flask_thres_tb = "CREATE TABLE IF NOT EXISTS Flask_thres_tb (Flask_conf_thres float, Flask_iou_thres float, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_thres.execute(Flask_thres_tb)  # open table

''' Sqlite---Flask_device '''
cur_idx_device = con_sql.cursor()  # create cursor
Flask_device_tb = "CREATE TABLE IF NOT EXISTS Flask_device_tb (Flask_device char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_device.execute(Flask_device_tb)  # open table


''' Sqlite---time '''
cur_idx_time = con_sql.cursor()  # create cursor
Img_tb = "CREATE TABLE IF NOT EXISTS Img_tb (from_path char, date_id char, time_id char, SN_id char, " \
         "img_num char, img_name char, Info char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_time.execute(Img_tb)  # open table

''' Sqlite---name '''
cur_idx_name = con_sql.cursor()  # create cursor
Img_tb = "CREATE TABLE IF NOT EXISTS Img_tb (from_path char, date_id char, time_id char, SN_id char, " \
         "img_num char, img_name char, Info char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_name.execute(Img_tb)  # open table


''' Sqlite---Train_Flask_epochs '''
cur_idx_T_epochs = con_sql.cursor()  # create cursor
T_Flask_epochs_tb = "CREATE TABLE IF NOT EXISTS T_Flask_epochs_tb (T_Flask_epochs char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_epochs.execute(T_Flask_epochs_tb)  # open table

''' Sqlite---Train_Flask_batchsize '''
cur_idx_T_batchsize = con_sql.cursor()  # create cursor
T_Flask_batchsize_tb = "CREATE TABLE IF NOT EXISTS T_Flask_batchsize_tb (T_Flask_batchsize char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_batchsize.execute(T_Flask_batchsize_tb)  # open table

''' Sqlite---Train_Flask_cfg '''
cur_idx_T_cfg = con_sql.cursor()  # create cursor
T_Flask_cfg_tb = "CREATE TABLE IF NOT EXISTS T_Flask_cfg_tb (T_Flask_cfg char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_cfg.execute(T_Flask_cfg_tb)  # open table

''' Sqlite---Train_Flask_weights '''
cur_idx_T_weights = con_sql.cursor()  # create cursor
T_Flask_weights_tb = "CREATE TABLE IF NOT EXISTS T_Flask_weights_tb (T_Flask_weights char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_weights.execute(T_Flask_weights_tb)  # open table

''' Sqlite---Train_Flask_name '''
cur_idx_T_name = con_sql.cursor()  # create cursor
T_Flask_name_tb = "CREATE TABLE IF NOT EXISTS T_Flask_name_tb (T_Flask_name char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_name.execute(T_Flask_name_tb)  # open table

''' Sqlite---Train_Flask_device '''
cur_idx_T_device = con_sql.cursor()  # create cursor
T_Flask_device_tb = "CREATE TABLE IF NOT EXISTS T_Flask_device_tb (T_Flask_device char, id integer primary key autoincrement)"  # create table of sqlite
cur_idx_T_device.execute(T_Flask_device_tb)  # open table

''' Sqlite---Metric '''
cur_idx_metric = con_sql.cursor()  # create cursor
Metric_tb = "CREATE TABLE IF NOT EXISTS Metric_tb (date_id char, time_id char, " \
            "Precise char, Recall char, mAP char, F1 char, " \
            "id integer primary key autoincrement)"  # create table of sqlite
cur_idx_metric.execute(Metric_tb)  # open table



''' ===================================== Sqlite——Flask 超参数 ===================================== '''
Read_Sqlite = True
Read_TXT = False
'''
NowSearch（用于判断是否切换到搜索状态）—— True时为搜索状态， False时为实时显示结果状态
TimeKey（搜索状态下，是否使用日期搜索）—— True时为是， False时为否
SNKey（搜索状态下，是否使用SN码搜索）—— True时为是， False时为否
PathKey（搜索状态下，是否使用文件夹搜索，这个文件夹指的是detect前的原始图片存放的文件夹）—— True时为是， False时为否
'''
# NowSearch = True
# TimeKey = False
# Vue_Search_DateKey = "2022-03-02"
# Vue_Search_TimeKey = "_14_30_17"
# SNKey = False
# Vue_Search_SNKey = "xxxxxxxxxxxxxx"
# PathKey = True
# Vue_Search_PathKey = "data/samples/111"

'''
Detect 部分
getmes_default —— True时，使用默认参数，False时，使用Sqlite中得最新数据（前端setting）
'''
getmes_default = False
Default_cfg = 'cfg/yolov3.cfg'
Default_weights = 'weights/0924-0.5115-best_d300-yolov3-smallset-iou0.2.pt'
Default_source = 'data/samples'
Default_output = 'output'
Default_conf_thres = 0.3
Default_iou_thres = 0.6
Default_device = ''

'''
Train 部分
Train_getmes_default —— True时，使用默认参数，False时，使用Sqlite中得最新数据（前端setting）
'''
Train_getmes_default = False
Train_Default_epochs = 300
Train_Default_batchsize = 4
Train_Default_cfg = 'cfg/yolov3.cfg'
Train_Default_weights = 'weights/darknet53.conv.74'
Train_Default_name = 'd300'
Train_Default_device = ''



# 用于识别最新文件夹, SN_id 原则上与 NewFolder 同步更新， 且一一对应
def new_report(test_report):
    lists = os.listdir(test_report)    # 列出目录的下所有文件和文件夹保存到lists
    lists.sort(key=lambda fn: os.path.getmtime(test_report + "/" + fn))    # 按时间排序
    file_new = os.path.join(test_report, lists[-1])   # 获取最新的文件保存到file_new
    file_new = test_report + '/' + lists[-1]
    # print(file_new)
    SN_id = "vvvvvvv---ooooooo"  # SN_id
    return file_new, SN_id


# 用于查询Sqlite中各表的统计量
def Find_Some():
    cur_upid = con_sql.cursor()
    cur_upid.execute("select * from sqlite_sequence")
    row_i = 0
    for column_i in cur_upid.fetchall():
        if row_i == 0:
            folder_sum = column_i[1]
            row_i += 1
        elif row_i == 1:
            dec_sum = column_i[1]
            row_i += 1
        elif row_i == 2:
            sta_sum = column_i[1]
            row_i += 1
        elif row_i == 3:
            outfolder_sum = column_i[1]
            row_i += 1
    # print("\n dec_sum === ", (dec_sum))
    # print("\n sta_sum === ", (sta_sum))
    # print("\n folder_sum === ", (folder_sum))
    # print("\n outfolder_sum === ", (outfolder_sum))
    return folder_sum, dec_sum, sta_sum, outfolder_sum


# 用于罗列 output 中的所有文件夹列表
def Read_OutFolder_tolist():
    _, _, _, outfolder_sum = Find_Some()
    print("\n\033[0;33m OutFolder_Sum ======> %s folders\033[0m" % (outfolder_sum))
    outfolder_all = dict()
    for fold_n in range(outfolder_sum):
        cur_idx_outfolder.execute("select * from OutFolder_tb WHERE id= '%s'" % (fold_n+1))
        outfolder_temp = cur_idx_outfolder.fetchone()
        outfolder_all.update({str(fold_n+1): outfolder_temp[1]})
    print("\n\033[0;33m OutFolder_list ======> %s\033[0m" % (outfolder_all))
    return outfolder_all


# 用于罗列 output 中锁定指定文件夹 folder 中的所有jpg文件列表
def OutFolder_File_tolist(outfolder):
    cur_idx_outfolder.execute("select * from OutFolder_tb WHERE out_path= '%s'" % (outfolder))
    for column_i in cur_idx_outfolder.fetchall():
        cnum = column_i[5]
        from_path = column_i[0]
    file_all =dict()
    for img_n in range(cnum):
        cur_idx.execute("select * from Img_tb WHERE from_path= '%s' and img_num= '%s'" % (from_path, str(img_n+1)))
        for column_i in cur_idx.fetchall():
            file_name = column_i[5]
            file_temp_v = outfolder + '/' + file_name + '.jpg'
        file_all.update({str(img_n+1): file_temp_v})
    print("\n\033[0;33m OutFolder_File_list ======> %s\033[0m" % (file_all))
    return file_all



if __name__ == '__main__':
    detect_path = new_report("data/samples")
    result_path = new_report("output")
